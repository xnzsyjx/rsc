// special functions module

// Laguerre polynomial
pub mod laguerre {
    use crate::constant;

    // Calculation of the normal Laguerre polynomial L(n, x)
    // ----------------------------------------------- 
    // The following recurrence relation is used : 
    // (k+1)*L(k+1, x) = (-x + 1 + 2k)*L(k, x) - k*L(k-1, x) for k > 0.
    // L(0, x) = 1. This value is directly returned if the degree of the polynomial is 0.
    // L(1, x) = 1 - x. This value is directly returned if the degree of the polynomial is 1.
    // for n < 0, L(-k, x) = e^x * L(k-1, -x) where k = -n
    pub fn normal(n:i32, x:f64) -> f64 {
        if n==0 {
            1.0
        } else if n>0 {
            let minus_x_plus_1: f64 = -x + 1.0;

            let mut poly_km1: f64;
            let mut poly_k: f64 = 1.0;
            let mut poly_kp1: f64 = minus_x_plus_1;

            for ik in 1..n{
                let k: f64 = ik as f64;
                
                poly_km1 = poly_k;
                poly_k = poly_kp1;
                poly_kp1 = ((minus_x_plus_1 + 2.0 * k) * poly_k - k * poly_km1)/(k + 1.0);
            }

            poly_kp1
        } else if n==-1{ // n < 0
            constant::E.powf(x)
        } else {
            let nabs_m_1: i32 = n.abs()-1;

            let x_plus_1: f64 = x + 1.0;

            let mut poly_km1: f64;
            let mut poly_k: f64 = 1.0;
            let mut poly_kp1: f64 = x_plus_1;

            for ik in 1..nabs_m_1{
                let k: f64 = ik as f64;
                
                poly_km1 = poly_k;
                poly_k = poly_kp1;
                poly_kp1 = ((x_plus_1 + 2.0 * k) * poly_k - k * poly_km1)/(k + 1.0);
            }

            poly_kp1*constant::E.powf(x)
        }
    }

    // Calculation of the generalized Laguerre polynomial L(n, x)
    // ----------------------------------------------- 
    // The following recurrence relation is used : 
    // (k+1)*L(k+1, a, x) = (-x + 1 + a + 2k)*L(k, x) - (k + a)*L(k-1, x) for k > 0.
    // L(0, a, x) = 1. This value is directly returned if the degree of the polynomial is 0.
    // L(1, a, x) = 1 + a - x. This value is directly returned if the degree of the polynomial is 1.
    pub fn generalize(n:i32, a:f64, x:f64) -> f64 {
        if n==0 {
            1.0
        } else if n>0 {
            let minus_x_plus_a_plus_1: f64 = -x + a + 1.0;

            let mut poly_km1: f64;
            let mut poly_k: f64 = 1.0;
            let mut poly_kp1: f64 = minus_x_plus_a_plus_1;

            for ik in 1..n{
                let k: f64 = ik as f64;
                
                poly_km1 = poly_k;
                poly_k = poly_kp1;
                poly_kp1 = ((minus_x_plus_a_plus_1 + 2.0 * k) * poly_k - (k + a) * poly_km1)/(k + 1.0);
            }

            poly_kp1
        } else { // n < 0
            0.0
        }
    }

    // Calculation of the k times derivatives of a Laguerre polynomial 
    // ----------------------------------------------- 
    // The following definition is used : 
    // d^k L(n, x) / d x^k = (-1)^k * L (n, k ,x) for k <= n,
    //                     = 0 , otherwise
    pub fn normal_der(n: i32, x: f64, k: i32) -> f64 {
        if k<=n {
            generalize(n-k, k as f64, x) * (if k%2==0 {1.0} else {-1.0})
        } else {
            0.0
        }
    }

    // Calculation of the k times derivatives of a generalized Laguerre polynomial 
    // ----------------------------------------------- 
    // The following definition is used : 
    // d^k L(n, a, x) / d x^k = (-1)^k * L (n, a+k ,x) for k <= n,
    //                        = 0 , otherwise
    pub fn generalize_der(n: i32, a: f64, x: f64, k: i32) -> f64 {
        if k<=n {
            generalize(n-k, a + k as f64, x) * (if k%2==0 {1.0} else {-1.0})
        } else {
            0.0
        }
    }
}

// Legendre polynomial
pub mod legendre {
    
}