// constants in scientifical calculation

//<====== PI ======>
pub const PI        : f64 =  3.1415926535897932; // pi
pub const PI_2      : f64 =  1.5707963267948966; // pi/2
pub const PI_3      : f64 =  1.0471975511965977; // pi/3
pub const PI_4      : f64 =  0.7853981633974483; // pi/4
pub const PI_5      : f64 =  0.6283185307179586; // pi/5
pub const PI_6      : f64 =  0.5235987755982989; // pi/6
pub const PI2       : f64 =  6.2831853071795865; // pi*2
pub const PI3       : f64 =  9.4247779607693797; // pi*3
pub const PI4       : f64 = 12.5663706143591730; // pi*4
pub const POW_PI2   : f64 =  9.8696044010893586; // pi^2
pub const POW_PI3   : f64 = 31.0062766802998202; // pi^3
pub const SQRT_PI   : f64 =  1.7724538509055160; // sqrt(pi)
pub const SQRT_2PI  : f64 =  2.5066282746310005; // sqrt(2*pi)
pub const SQRT3_PI  : f64 =  1.46459188756152326; // cube root of pi
pub const ONE_PI    : f64 =  0.3183098861837907; // 1/pi
pub const TWO_PI    : f64 =  0.6366197723675813; // 2/pi
pub const ONE_SPI   : f64 =  0.5641895835477563; // 1/sqrt(pi)
pub const ONE_S2PI  : f64 =  0.3989422804014327; // 1/SQRT(2*pi)

//<===== e =====>
pub const E         : f64 =  2.7182818284590452; // e
pub const E_2       : f64 =  1.3591409142295226; // e/2
pub const E2        : f64 =  5.4365636569180905; // 2*e
pub const POW_E2    : f64 =  7.3890560989306502; // e^2
pub const POW_E3    : f64 = 20.0855369231876677; // e^3
pub const POW_EPI   : f64 = 23.1406926327792690; // e^pi
pub const POW_EG    : f64 =  1.7810724179901980; // e^gamma, gamma is the derivative of Γ(x) at x=1
pub const POW_EE    : f64 = 15.1542622414792642; // e^e
pub const SQRT_E    : f64 =  1.6487212707001281; // sqrt(e)
pub const SQRT_PEPI : f64 =  4.8104773809653517; // sqrt(e^pi)
pub const ONE_E     : f64 =  0.3678794411714423; // 1/e
pub const ONR_PEPI  : f64 =  0.0432139182637722; // 1/(e^pi)
pub const ONE_PEG   : f64 =  0.5614594835668852; // 1/(e^gamma), gamma is the derivative of Γ(x) at x=1
pub const ONE_PEE   : f64 =  0.0659880358453125; // 1/(e^e)
pub const ONE_SE    : f64 =  0.6065306597126334; // 1/sqrt(e)
pub const ONE_SPEPI : f64 =  0.2078795763507619; // 1/sqrt(e^pi)

//<===== ln(x) =====>
pub const LN2       : f64 =  0.6931471805599453; // ln(2)
pub const LN3       : f64 =  1.0986122886681097; // ln(3)
pub const LN5       : f64 =  1.6094379124341004; // ln(5)
pub const LN7       : f64 =  1.9459101490553133; // ln(7)
pub const LN10      : f64 =  2.3025850929940457; // ln(10)
pub const ONE_LN10  : f64 =  0.4342944819032518; // 1/ln(10)
pub const LN2_LN3   : f64 =  0.63092975357145743709953; // ln(2)/ln(3)
pub const LNLN2     : f64 = -0.36651292058166432701244; // ln( ln(2) )
pub const LNPI      : f64 =  1.14472988584940017414343; // ln(pi)
pub const LNSPI2    : f64 =  0.91893853320467274178033; // ln( sqrt(pi*2) )
pub const LNGAMMA   : f64 = -0.54953931298164482233766; // ln(gamma), gamma is the derivative of Γ(x) at x=1
pub const LNPHI     : f64 =  0.48121182505960344749776; // ln(phi), phi=(1+sqrt(5))/2 is the golden ratio

//<===== Γ(x) =====>
pub const GAMMA     : f64 =  0.57721566490153286060651; // -γ is the derivative value of Γ(x) at x=1
pub const ONE_GAMMA : f64 =  1.73245471460063347358303; // 1/γ
pub const GAMMAHALF : f64 =  1.77245385090551602729817; // Γ(1/2)

//<===== constants for calculation =====>
pub const PRECISION : f64 =  1e-16;