// special number module
// ----------------------------------------------
// this module aim to get some special number used during calculation
// for example, Eulerian number
use crate::fact;
use std::process;

// Eulerian number A(n,k)
// A(n,k) = sum_{i=0}^k (-1)^i * C(n+1,i) * (k+1-i)^n
// where C(n,m) is binomial coefficient
// ----------------------------------------------
pub fn eulerian(n: i32, k: i32) -> f64 {
    if n<0 || k>=n {
        eprintln!("\nError !!!\nn > k >= 0 in Eulerian number calculation A(n, k)\n");
        process::exit(0)
    } else if k==0 || n==k+1 {
        1.0
    } else {
        let mut sign: f64 = 1.0;
        let mut res: f64 = 0.0;
        let np1: f64 = (n+1) as f64; 
        let kp1: f64 = (k+1) as f64;
        let n_f64: f64 = n as f64;
        for i in 0..=k {
            res += sign*fact::binomial(np1,i)*(kp1-i as f64).powf(n_f64);
            sign *= -1.0;
        }
        res
    }
}